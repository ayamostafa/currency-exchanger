import {IAmountDescription} from "./IAmountDescription";

export interface ICurrenciesContext {
    amountsList: IAmountDescription[];
    fromCurrency: string;
    toCurrency: string;
    amountFrom: number;
    amountTo: number;
    isLoadingCurrencies:boolean;
    currenciesList:any;
    isSameBaseCurrency:boolean;
    symbols:any;
    setFromCurrency: any;
    setToCurrency: any;
    setAmountFrom: any;
    setAmountTo: any;
    setIsSameBaseCurrency: any;
}
