export interface IAmountDescription{
    from: string;
    to: string;
    amountFrom: number;
    amountTo: number
}
