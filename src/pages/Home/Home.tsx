import {Link} from "react-router-dom";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faAngleRight} from '@fortawesome/free-solid-svg-icons'
import {IconProp} from '@fortawesome/fontawesome-svg-core';
import Converter from "../../components/Converter/Converter";
import AmountsList from "../../components/AmountsList/AmountsList";

const faAngleRightIcon = faAngleRight as IconProp;

const Home = () => {
    return <>
        <Converter/>
        <Link to="/details" className='details-link'>
            Go to More Details <FontAwesomeIcon icon={faAngleRightIcon}/>
        </Link>
        <AmountsList/>
    </>
}
export default Home;
