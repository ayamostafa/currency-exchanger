import React, {useContext} from "react";
import {CurrenciesContext} from "../../contexts/CurrenciesContext";
import {Link} from "react-router-dom";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faAngleLeft} from '@fortawesome/free-solid-svg-icons'
import {IconProp} from '@fortawesome/fontawesome-svg-core';
import CurrenciesList from "../../components/CurrenciesList/CurrenciesList";
import './Details.scss';
import HistoricalCurrency from "../../components/HistoricalCurrency/HistoricalCurrency";

const faAngleLeftIcon = faAngleLeft as IconProp;

const Details = () => {
    const {symbols, fromCurrency, toCurrency} = useContext(CurrenciesContext);
    return <div className='details'>
        <CurrenciesList/>
        <div className='details-desc'>
            <p><span className='title'>From:</span>{symbols[fromCurrency]}</p>
            <p><span className='title'>To:</span>{symbols[toCurrency]}</p>
        </div>
        <HistoricalCurrency/>
        <Link to="/" className='details-link'>
            <FontAwesomeIcon icon={faAngleLeftIcon}/>&nbsp;&nbsp;Back to Home
        </Link>
    </div>
}
export default Details;
