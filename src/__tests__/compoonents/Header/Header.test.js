import {shallow} from "enzyme";
import Header from "../../../components/Header/Header";

describe('<Header />', () => {
    it('renders <Header /> component without errors', () => {
        let wrapper = shallow(<Header/>);
        expect(wrapper).toMatchSnapshot();
    });
});
