import {shallow} from "enzyme";
import Converter from "../../../components/Converter/Converter";

describe('<Converter />', () => {
    it('renders <Converter /> component without errors', () => {
        let wrapper = shallow(<Converter/>);
        expect(wrapper).toMatchSnapshot();
    });
});
