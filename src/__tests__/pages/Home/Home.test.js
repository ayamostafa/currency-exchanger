import {shallow} from "enzyme";
import Home from "../../../pages/Home/Home";

describe('<Home />', () => {
    it('renders <Home /> component without errors', () => {
        const wrapper = shallow(<Home/>);
        expect(wrapper).toMatchSnapshot();
    });
});
