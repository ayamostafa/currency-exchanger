import React from 'react';
import ReactDOM from 'react-dom';
import Home from "./pages/Home/Home";
import {BrowserRouter} from "react-router-dom";
import {Routes, Route} from "react-router-dom";
import reportWebVitals from './reportWebVitals';
import './assets/styles/styles.scss';
import Details from "./pages/Details/Details";
import Header from "./components/Header/Header";
import CurrenciesProvider from "./contexts/CurrenciesContext";

ReactDOM.render(
    <React.StrictMode>
        <BrowserRouter>
            <div className='page'>
                <Header/>
                <CurrenciesProvider>
                <Routes>
                    <Route path="/" element={<Home/>}/>
                    <Route path="details" element={<Details/>}/>
                </Routes>
                </CurrenciesProvider>
            </div>
        </BrowserRouter>
    </React.StrictMode>,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
