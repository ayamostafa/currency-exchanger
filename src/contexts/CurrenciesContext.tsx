import React, {createContext, useEffect, useState} from "react";
import {IAmountDescription} from "../models/IAmountDescription";
import {ICurrenciesContext} from "../models/ICurrenciesContext";
import {BaseCurrency, DefaultToCurrency} from "../utils/config";
import {getCurrenciesApi} from "../apis/currency-api";
import {getSymbolsApi} from "../apis/symbols-api";

const defaultState = {
    amountsList: [],
    fromCurrency: BaseCurrency,
    toCurrency: DefaultToCurrency,
    amountFrom: 1,
    amountTo: -1,
    isLoadingCurrencies: false,
    currenciesList: {},
    isSameBaseCurrency: false,
    symbols: {},
    setFromCurrency: () => {
    },
    setToCurrency: () => {
    },
    setAmountFrom: () => {
    },
    setAmountTo: () => {
    },
    setIsSameBaseCurrency: () => {
    }

};
export const CurrenciesContext = createContext<ICurrenciesContext>(defaultState);

const CurrenciesProvider = ({children}: any) => {
    const [currenciesList, setCurrenciesList] = useState<any>({});
    const [isSameBaseCurrency, setIsSameBaseCurrency] = useState<boolean>(false);
    const [amountsList, setAmountsList] = useState<Array<IAmountDescription>>([]);
    const [fromCurrency, setFromCurrency] = useState<string>(BaseCurrency);
    const [toCurrency, setToCurrency] = useState<string>(DefaultToCurrency);
    const [amountFrom, setAmountFrom] = useState<number>(1);
    const [amountTo, setAmountTo] = useState<number>(-1);
    const [isLoadingCurrencies, setIsLoadingCurrencies] = useState<boolean>(false);
    const [symbols, setSymbols] = useState<any>({});

    useEffect(() => {
        setIsLoadingCurrencies(true);
        getCurrenciesApi(fromCurrency)
            .then(function (response: any) {
                setAmountTo(amountFrom * response.data.data[toCurrency]);
                setIsSameBaseCurrency(true);
                setCurrenciesList({...response.data.data, [fromCurrency]: 1})//push the base currency
                setIsLoadingCurrencies(false);
            })
            .catch(function (error: any) {
                alert("Something Wrong! Please, try again!");
                setIsLoadingCurrencies(false);
            })
        getSymbolsApi().then(res => {
            setSymbols(res.data.symbols);
        }).catch(e => {
            console.log(e);
            alert("Something Wrong! Please, try again!");
        })
    }, []);

    useEffect(() => {
        if (amountTo > -1) {
            setAmountsList((prev: IAmountDescription[]) => [...prev, {
                from: fromCurrency, to: toCurrency, amountFrom: amountFrom, amountTo: amountTo
            }]);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [amountTo])
    return (<CurrenciesContext.Provider value={{
        amountsList,
        fromCurrency,
        toCurrency,
        amountFrom,
        amountTo,
        isLoadingCurrencies,
        currenciesList,
        isSameBaseCurrency,
        symbols,
        setFromCurrency,
        setToCurrency,
        setAmountFrom,
        setAmountTo,
        setIsSameBaseCurrency
    }}>
        {children}
    </CurrenciesContext.Provider>)
}
export default CurrenciesProvider;
