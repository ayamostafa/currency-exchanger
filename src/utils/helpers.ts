const getDateFormatted = (date:Date)=>{
   return date.toISOString().slice(0, 10);
}

export {
    getDateFormatted
}
