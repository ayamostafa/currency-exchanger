const {REACT_APP_API_BASE_URL, REACT_APP_API_KEY, REACT_APP_APP_NAME} = process.env;

const AppName = REACT_APP_APP_NAME;
const BaseCurrency = 'USD';
const DefaultToCurrency = 'EGP';
const PredefinedCurrencies = [
    'USD',
    'EGP',
    'EUR'
];
const ApiKey = REACT_APP_API_KEY;
const ApiBaseUrl = REACT_APP_API_BASE_URL + '/api/v2';

export {
    AppName,
    BaseCurrency,
    DefaultToCurrency,
    PredefinedCurrencies,
    ApiKey,
    ApiBaseUrl
}
