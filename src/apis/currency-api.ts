import axios from "axios";
import {ApiBaseUrl, ApiKey} from '../utils/config'

const getCurrenciesApi = (fromCurrency:string) => {
    return axios(`${ApiBaseUrl}/latest?apikey=${ApiKey}&base_currency=${fromCurrency}`)
}
export {
    getCurrenciesApi
}
