import axios from "axios";
import {ApiBaseUrl, ApiKey} from '../utils/config'

const getHistoricalApi = (fromCurrency:string,fromDate:string,toDate:string) => {
    return axios(`${ApiBaseUrl}/historical?apikey=${ApiKey}&base_currency=${fromCurrency}&date_from=${fromDate}&date_to=${toDate}`)
}
export {
    getHistoricalApi
}
