import {useContext, useState, useEffect} from "react";
import {CurrenciesContext} from "../../contexts/CurrenciesContext";
import {useLocation} from 'react-router-dom'
import './CurrenciesList.scss';

const CurrenciesList = () => {
    const {
        currenciesList,
        fromCurrency,
        setFromCurrency,
        toCurrency,
        setToCurrency,
        setIsSameBaseCurrency,
    } = useContext(CurrenciesContext);

    const location = useLocation();
    useEffect(() => {
        if (location.pathname === '/details') {
            setIsDetailsPage(true);
        } else {
            setIsDetailsPage(false)
        }
    }, [location.pathname]);

    const [isDetailsPage, setIsDetailsPage] = useState(false);
    return <>
        <div className='selectors currencies'>
            <div>
                {!isDetailsPage && <p className='title'>I have</p>}
                {Object.keys(currenciesList).length > 0 &&
                    <select disabled={isDetailsPage} onChange={e => {
                        setFromCurrency(e.target.value);
                        setIsSameBaseCurrency(false);
                    }} value={fromCurrency}>
                        {Object.keys(currenciesList).map((currency, idx) =>
                            <option key={idx} value={currency}>{currency}</option>)
                        }
                    </select>}
            </div>
            <div>
                {!isDetailsPage && <p className='title'>I want</p>}
                {Object.keys(currenciesList).length > 0 &&
                    <select disabled={isDetailsPage} onChange={e => {
                        setToCurrency(e.target.value)
                    }} value={toCurrency}>
                        {Object.keys(currenciesList).map((currency, idx) => <option key={idx}
                                                                                    value={currency}>{currency}</option>)}
                    </select>}
            </div>
        </div>
    </>
}
export default CurrenciesList;
