import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faSpinner} from "@fortawesome/free-solid-svg-icons";
import {IconProp} from "@fortawesome/fontawesome-svg-core";
const faSpinnerIcon = faSpinner as IconProp;

interface loaderProps{
    tip?:string;
}
const Loader = ({tip}:loaderProps)=>{
    return  <span><FontAwesomeIcon className='spinner' icon={faSpinnerIcon}/> {tip}</span>
}
export default Loader;
