import {useContext, useState, useEffect} from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {IconProp} from "@fortawesome/fontawesome-svg-core";
import {faList, faHandPointRight} from "@fortawesome/free-solid-svg-icons";
import {CurrenciesContext} from "../../contexts/CurrenciesContext";
import {getHistoricalApi} from "../../apis/historical-api";
import {PredefinedCurrencies} from "../../utils/config";
import {getDateFormatted} from "../../utils/helpers";
import './HistoricalCurrency.scss';
import Loader from "../Loader/Loader";

const faListIcon = faList as IconProp;
const faHandPointRightIcon = faHandPointRight as IconProp;
let toDate = new Date();
let fromDate = new Date();
let pastYear = toDate.getFullYear() - 1;
fromDate.setFullYear(pastYear);
toDate.setFullYear(pastYear);

let tempSelectedCurrencies: any = [];
const HistoricalCurrency = () => {
    const {fromCurrency, amountsList} = useContext(CurrenciesContext);
    const [lastYearRates, setLastYearRates] = useState<any>({});
    const [selectedCurrencies, setSelectedCurrencies] = useState<any>([])
    const [isLoadingData, setIsLoadingData] = useState<boolean>(false);
    const [preCurrenciesMissed, setPreCurrenciesMissed] = useState<{ loadedAll: boolean, data: [] }>({
        loadedAll: false,
        data: []
    });

    useEffect(() => {
        for (let i = 0; i < amountsList.length; i++) {
            tempSelectedCurrencies.push(amountsList[i].from)
        }
        setSelectedCurrencies(Array.from(new Set(tempSelectedCurrencies)));
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
    useEffect(() => {
        if (selectedCurrencies.length > 0) {
            for (let i = 0; i < selectedCurrencies.length; i++) {
                getHistoricalApi(selectedCurrencies[i], getDateFormatted(fromDate), getDateFormatted(toDate)).then(res => {
                    setLastYearRates((prev: any) => {
                        return {...prev, [selectedCurrencies[i]]: res.data.data[getDateFormatted(fromDate)]}
                    })
                }).catch(() => {
                    alert("Something went wrong! Please, try again");
                    setLastYearRates((prev: any) => {
                        return {...prev, undefined: {}}
                    })
                })
            }
        }
    }, [selectedCurrencies]);
    useEffect(() => {
        if (preCurrenciesMissed.loadedAll && preCurrenciesMissed.data.length > 0 && Object.keys(lastYearRates).length < selectedCurrencies.length + preCurrenciesMissed.data.length) {
            for (let i = 0; i < preCurrenciesMissed.data.length; i++) {
                getHistoricalApi(preCurrenciesMissed.data[i], getDateFormatted(fromDate), getDateFormatted(toDate)).then(res => {
                    setLastYearRates((prev: any) => {
                        return {...prev, [preCurrenciesMissed.data[i]]: res.data.data[getDateFormatted(fromDate)]}
                    })
                }).catch(() => {
                    alert("Something went wrong! Please, try again");
                    setLastYearRates((prev: any) => {
                        return {...prev, undefined: {}}
                    })
                })
            }
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [preCurrenciesMissed]);
    useEffect(() => {
        if (preCurrenciesMissed.loadedAll && Object.keys(lastYearRates).length === (selectedCurrencies.length + preCurrenciesMissed.data.length)) {
            setIsLoadingData(false);
        }
        if (!preCurrenciesMissed.loadedAll && Object.keys(lastYearRates).length === selectedCurrencies.length && selectedCurrencies.length > 0) {
            for (let i = 0; i < PredefinedCurrencies.length; i++) {
                if (Object.keys(lastYearRates).findIndex(currency => currency === PredefinedCurrencies[i]) === -1) { //predefined currency data not found in current list
                    setPreCurrenciesMissed((prev: any) => {
                        return {...prev, data: [...prev.data, PredefinedCurrencies[i]]}
                    });
                }
                if (i === PredefinedCurrencies.length - 1) { //last iteration
                    setPreCurrenciesMissed(prev => {
                        return {...prev, loadedAll: true}
                    })
                }
            }
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [lastYearRates]);
    return <>
        {isLoadingData ? <Loader tip="Loading Historical Data"/> :
            <div className='historical'>
                <h3 className='title'>
                    <FontAwesomeIcon icon={faListIcon} className='fa-xs'/> Last
                    Year<span>({getDateFormatted(fromDate)})</span> the
                    historical exchange rates for:-
                </h3>
                <div className='historical-list'>
                    {amountsList.map((amount, idx) =>
                        <p key={idx}>
                            <FontAwesomeIcon icon={faHandPointRightIcon}/>&nbsp;
                            <b>{amount.from}</b> to <b>{amount.to}</b> =   {lastYearRates[amount.from] ? lastYearRates[amount.from][amount.to]:''} with
                            your amount<b> {amount.amountFrom}</b> =
                            <span
                                className='historical-amount'>{lastYearRates[amount.from]? amount.amountFrom * lastYearRates[amount.from][amount.to]: 'Not Found'} </span>
                        </p>
                    )}
                </div>

                <h3 className='title'>
                    <FontAwesomeIcon icon={faListIcon} className='fa-xs'/> Last
                    Year<span>({getDateFormatted(fromDate)})</span> the exchange rate from {fromCurrency} to the
                    following popular currencies:-
                </h3>
                <div className='historical-list'>
                    {PredefinedCurrencies.map((predefinedCurrency, idx) =>
                        <p key={idx}>
                            <FontAwesomeIcon icon={faHandPointRightIcon}/>&nbsp;
                            from <b>{fromCurrency}</b> to <b>{predefinedCurrency}</b> =
                            <span
                                className='historical-amount'> {lastYearRates[fromCurrency]? 1 * lastYearRates[fromCurrency][predefinedCurrency]:'Not Found'}</span>
                        </p>
                    )}
                </div>

                <h3 className='title'>
                    <FontAwesomeIcon icon={faListIcon} className='fa-xs'/> Last
                    Year<span>({getDateFormatted(fromDate)})</span> the exchange rate for the following popular
                    currencies:-
                </h3>
                <div className='historical-list'>
                    {PredefinedCurrencies.map((predefinedCurrency, idx) =>
                        <p key={idx}>
                            <FontAwesomeIcon icon={faHandPointRightIcon}/>&nbsp;
                            from <b>{predefinedCurrency}</b> to <b>{fromCurrency}</b> =
                            <span
                                className='historical-amount'> {lastYearRates[predefinedCurrency] ? 1 * lastYearRates[predefinedCurrency][fromCurrency]:'Not Found'}</span>
                        </p>
                    )}
                </div>
            </div>
        }
    </>
}

export default HistoricalCurrency;
