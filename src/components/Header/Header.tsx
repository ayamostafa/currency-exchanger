import {Link} from "react-router-dom";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faEuroSign, faDollarSign} from '@fortawesome/free-solid-svg-icons'
import {IconProp} from '@fortawesome/fontawesome-svg-core';
import LogoImg from '../../assets/images/logo.png';
import {AppName} from "../../utils/config";
import './Header.scss';

const faDollarIcon = faDollarSign as IconProp;
const faEuroIcon = faEuroSign as IconProp;

const Header = () => {
    return <div className='header'>
        <nav className='header-nav'>
            <Link to="/">
                <img className='logo' src={LogoImg} alt='Bank Misr'/>
            </Link>
            <h2 className='app-title'>{AppName}</h2>
            <div className='currency-nav'>
                <Link state={{
                    currency: 'usd'
                }} to={{
                    pathname: "/details",
                }} className='currency-link'>
                    <FontAwesomeIcon icon={faDollarIcon}/>
                </Link>
                <Link state={{
                    currency: 'euro'
                }}
                      to={{
                          pathname: "/details",
                      }} className='currency-link'>
                    <FontAwesomeIcon icon={faEuroIcon}/>
                </Link>
            </div>
        </nav>
    </div>
}
export default Header;
