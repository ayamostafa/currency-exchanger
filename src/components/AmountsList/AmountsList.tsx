import React, {useContext} from "react";
import {CurrenciesContext} from "../../contexts/CurrenciesContext";
import './AmountsList.scss';

const AmountsList = () => {
    const {amountsList} = useContext(CurrenciesContext);
    return <>
        {amountsList.length > 0 && <table className='amounts-list'>
            <thead>
            <tr>
                <th>#</th>
                <th>From</th>
                <th>To</th>
                <th>Amount From</th>
                <th>Amount To</th>
            </tr>
            </thead>
            <tbody>
            {amountsList.map((amount, idx) => <tr key={idx} className='amount'>
                <td>{idx + 1}</td>
                <td>{amount.from}</td>
                <td>{amount.to}</td>
                <td>{amount.amountFrom}</td>
                <td>{amount.amountTo}</td>
            </tr>)}
            </tbody>
        </table>}
    </>
}
export default AmountsList;
