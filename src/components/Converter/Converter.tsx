import {ChangeEvent, useCallback, useState, useContext} from "react";
import {debounceTime, Subject} from "rxjs";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faArrowRightArrowLeft} from '@fortawesome/free-solid-svg-icons'
import {IconProp} from '@fortawesome/fontawesome-svg-core';
import {CurrenciesContext} from "../../contexts/CurrenciesContext";
import './Converter.scss';
import {getCurrenciesApi} from "../../apis/currency-api";
import CurrenciesList from "../CurrenciesList/CurrenciesList";
import Loader from "../Loader/Loader";

const faArrowRightArrowLeftIcon = faArrowRightArrowLeft as IconProp;

let values$: any = new Subject().pipe(debounceTime(500));
let convertAmount: () => void;
values$.subscribe(() => {
    convertAmount();
})

const Converter = () => {
    const {
        fromCurrency,
        toCurrency,
        amountFrom,
        amountTo,
        setAmountFrom,
        setAmountTo,
        setFromCurrency,
        setToCurrency,
        isLoadingCurrencies,
        currenciesList,
        isSameBaseCurrency,
        setIsSameBaseCurrency
    } = useContext(CurrenciesContext);

    const [isLoadingNewBase, setIsLoadingNewBase] = useState<boolean>(false);
    const [newCurrenciesList, setNewCurrenciesList] = useState<any>({});

    const getCurrenciesData = () => {
        getCurrenciesApi(fromCurrency)
            .then(function (response: any) {
                setAmountTo(amountFrom * response.data.data[toCurrency]);
                setIsSameBaseCurrency(true);
                setNewCurrenciesList(response.data.data);
                setIsLoadingNewBase(false);
            })
            .catch(function (error: any) {
                alert("Something Wrong! Please, try again!");
                console.log(error);
                setIsLoadingNewBase(false);
            })
    }

    convertAmount = useCallback(() => {
        if (!isSameBaseCurrency) { //if base currency changed in this case =>then call api
            setIsLoadingNewBase(true);
            getCurrenciesData();
        } else {
            if (Object.keys(newCurrenciesList).length === 0) { //not set before
                setAmountTo(amountFrom * currenciesList[toCurrency])
            } else {
                setAmountTo(amountFrom * newCurrenciesList[toCurrency])
            }
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [amountFrom, isSameBaseCurrency, fromCurrency, toCurrency, currenciesList]);


    const changeFromHandler = (e: ChangeEvent<HTMLInputElement>) => {
        if (Number(e.target.value) >= 1) {
            setAmountFrom(Number(e.target.value));
            values$.next(Number(e.target.value));
        }
    }

    const swapCurrencies = () => {
        let tempFromCurrency = fromCurrency;
        setFromCurrency(toCurrency);
        setToCurrency(tempFromCurrency);
        setIsSameBaseCurrency(false);
    }
    return <div className='converter'>
        {isLoadingCurrencies ? <Loader tip="Loading Currencies"/> : <>
                <CurrenciesList/>
                <div className='selectors amounts'>
                    <div>
                        <input type='number' min={1} className='from-converter' value={amountFrom}
                               placeholder='Please, Enter the Amount to converted'
                               autoFocus={true}
                               onChange={e => changeFromHandler(e)}
                        />
                    </div>
                    <button className='exchanger-icon' onClick={swapCurrencies}>
                        <FontAwesomeIcon icon={faArrowRightArrowLeftIcon}/>
                    </button>
                    <div>
                        <input type='number' min={1} value={amountTo}
                               readOnly={true} disabled={true}/>
                    </div>
                </div>
                <div className='converter-btn'>
                    <button data-testid='btn-convert' disabled={isLoadingNewBase} onClick={convertAmount}>
                        {isLoadingNewBase && <Loader />}
                        Convert
                    </button>
                </div>
            </>}
    </div>
}
export default Converter;
